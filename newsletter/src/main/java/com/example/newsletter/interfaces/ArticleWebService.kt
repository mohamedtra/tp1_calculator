package com.example.newsletter.interfaces;

import com.example.newsletter.data.repositories.ArticleResponse;
import com.example.newsletter.data.repositories.SourceResponse
import retrofit2.Call


import retrofit2.http.GET
import retrofit2.http.Query


interface ArticleWebService {


    @GET("/v2/everything")
    fun list(): Call<ArticleResponse>

    @GET("/v2/top-headlines")
    fun getFilteredArticles(
        @Query("country")
        country: String,
        @Query("category")
        category: String,
        @Query("source")
        source: String
    ): Call<ArticleResponse>

    @GET("/v2/top-headlines")
    fun getFilteredArticlesBySource(
        @Query("sources")
        source: String
    ): Call<ArticleResponse>

    @GET("/v2/top-headlines")
    fun getFilteredArticlesByCountry(@Query("country") country: String): Call<ArticleResponse>

    @GET("/v2/top-headlines")
    fun getFilteredArticlesByCategory(@Query("category") category: String): Call<ArticleResponse>

    @GET("/v2/sources")
    fun get(
        @Query("sources")
        sources: String
    ): Call<ArticleResponse>


    @GET("/v2/sources")
    fun getSources(
    ): Call<SourceResponse>
}