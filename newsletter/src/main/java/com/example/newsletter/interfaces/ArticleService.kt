package com.example.newsletter.interfaces

import com.example.newsletter.data.repositories.ArticleResponse
import com.example.newsletter.entity.Article
import retrofit2.Call


interface ArticleService {
    fun getArticles(): ArticleResponse?

    fun getFilteredArticles(country: String, category: String): ArticleResponse?

    fun getSourceArticles(sources: String): ArticleResponse?
}