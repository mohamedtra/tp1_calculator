package com.example.newsletter.interfaces

import com.example.newsletter.data.repositories.ArticleResponse
import com.example.newsletter.entity.Article

import retrofit2.Call
import retrofit2.Response

import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitApiService {
    //GET --> On lance une requête de type GET
    // everything est l'action du web service qu'on veut apeler
    // Elle sera concaténée avec l'url prédéfini dans retrofit
    @GET("/everything")
    fun list(): Call<ArticleResponse>
    @GET("/top-headlines")
    fun getFilteredArticles(
        @Query("country")
        country: String,
        @Query("category")
        category: String
    ): Call<ArticleResponse>

    @GET("/sources")
    fun getSourceArticles(
        @Query("sources")
        sources: String
    ): Call<ArticleResponse>

}