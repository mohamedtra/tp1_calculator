package com.example.newsletter.fragments.views

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CompoundButton
import androidx.core.view.children
import androidx.core.view.forEach
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.example.newsletter.MainActivity
import com.example.newsletter.R
import com.example.newsletter.adapters.ArticleAdapter
import com.example.newsletter.adapters.CountryAdapter
import com.example.newsletter.adapters.ViewPagerAdapter
import com.example.newsletter.data.repositories.ArticleResponse
import com.example.newsletter.databinding.FragmentArticleByCategoryBinding
import com.example.newsletter.entity.Article
import com.example.newsletter.entity.Category
import com.example.newsletter.entity.Country
import com.example.newsletter.repositories.ArticleWebRepository
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.tabs.TabLayout
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CountryFragment: Fragment() {

    private var lastId: Int = 0
    lateinit var chipGroup: ChipGroup
    var selectedCategory: String = "general"
    var selectedCountry: String = "general"



    private lateinit var listArticles: ArticleResponse

    private lateinit var v: View
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        /*articleAdapter.setOnItemClickListener {
            val bundle = Bundle().apply {
                putSerializable("article", it)
            }
        }*/
        // Inflate the layout for this fragment
//        binding = FragmentArticleByCategoryBinding.inflate(inflater, container, false)
//        return  binding.root
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_article_by_country, container, false)


        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val countries = listOf<Country>(
            Country("AE","AE",1),
            Country("AR","AR",2),
            Country("AT","AT",3),
            Country("AU","AU",4),
            Country("BE","BE",5),
            Country("BG","BG",6),
            Country("BR","BR",7),
            Country("CA","CA",8),
            Country("CH","CH",9),
            Country("CN","CN",10),
            Country("CO","CO",11),
            Country("CU","CU",12),
            Country("CZ","CZ",13),
            Country("DE","DE",14),
            Country("EG","EG",15),
            Country("FR","FR",16),
            Country("GB","GB",17),
            Country("GR","GR",18),
            Country("HK","HK",19),
            Country("HU","HU",20),
            Country("ID","ID",22),
            Country("IE","IE",23),
            Country("IL","IL",24),
            Country("IN","IN",25),
            Country("IT","IT",26),
            Country("JP","JP",27),
            Country("KR","KR",28),
            Country("LT","LT",29),
            Country("LV","LV",30),
            Country("MA","MA",31),
            Country("MX","MX",32),
            Country("MY","MY",33),
            Country("NG","NG",34),
            Country("NL","NL",35),
            Country("NO","NO",36),
            Country("PH","PH",37),
            Country("PL","PL",38),
            Country("PT","PT",39),
            Country("RO","RO",40),
            Country("RS","RS",41),
            Country("RU","RU",42),
            Country("SA","SA",43),
            Country("SE","SE",44),
            Country("SG","SG",45),
            Country("SI","SI",46),
            Country("SK","SK",47),
            Country("TH","TH",48),
            Country("TR","TR",49),
            Country("TW","TW",50),
            Country("UA","UA",51),
            Country("US","US",52),
            Country("VE","VE",53),
            Country("ZA","ZA",54)
        )


        chipGroup = view.findViewById(R.id.filter_list_values)

        lifecycleScope.launch {
            withContext(Dispatchers.IO) {

                countries.forEach {
                    val chip: Chip = Chip(chipGroup.context)
                    chip.setText(it.name)
                    chip.setTag(it.id)
                    chip.setCheckable(true)

                    activity?.runOnUiThread(java.lang.Runnable {
                        chipGroup.addView(chip)
                    })
                }
            }
        }


      chipGroup.setOnCheckedChangeListener(ChipGroup.OnCheckedChangeListener { chipGroup: ChipGroup, i: Int ->
            Log.i("id", "currentId $id")
              if(i != lastId) {
                  lastId = i

                  val chip: Chip = chipGroup.findViewById(i)
                  val country = chip?.text.toString()
                  Thread {
                      (parentFragment as NewsFragment).getData(country = country)
                  }.start()
              }
        })
    }

}
