package com.example.newsletter.fragments.views


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.replace
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.newsletter.MainActivity
import com.example.newsletter.R
import com.example.newsletter.data.adapters.ArticleListAdapter
import com.example.newsletter.data.models.ArticleViewModel
import com.example.newsletter.entity.Article
import com.example.newsletter.fragments.ArticleDetailsFragment

class FavoritesFragment: Fragment(R.layout.fragment_view_favorites) {


    lateinit var viewModel: ArticleViewModel
    lateinit var articleListAdapter: ArticleListAdapter
    private lateinit var v: View
    private lateinit var viewArticleFrameLayout: FrameLayout



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = (activity as MainActivity).articleViewModel
        v = inflater.inflate(R.layout.fragment_view_favorites, container, false)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getData()

        viewArticleFrameLayout = view.findViewById(R.id.view_article)

    }
    fun hideArticleDetailFragment(){
        viewArticleFrameLayout.setVisibility(View.GONE)
    }
    fun getData() {

        val result = viewModel.getAllArticles()
        Log.d("results", result.value.toString())

        viewModel.getAllArticles().observe(viewLifecycleOwner, Observer { articles ->
            articleListAdapter = ArticleListAdapter(articles, viewModel)

            articleListAdapter.differ.submitList(articles)
            activity?.runOnUiThread(java.lang.Runnable {
                bindData(articles)
            })
        } )
    }


    private fun bindData(result: List<Article>) {
        //afficher les données dans le recycler
        var recyclerView: RecyclerView = v.findViewById(R.id.article2_card_list)

       articleListAdapter.setOnViewBtnClickListener {
            val adf = it?.let { it1 -> ArticleDetailsFragment.newInstance(it1) }
            if (adf != null) {
                childFragmentManager.beginTransaction()
                    .replace(R.id.view_article, adf)
                    .commitAllowingStateLoss()
            }
            viewArticleFrameLayout.setVisibility(View.VISIBLE)
            Log.d("test",it?.title as String)
        }

        //articleListAdapter = ArticleListAdapter(result, viewModel)
        recyclerView.hasFixedSize()

        recyclerView.layoutManager = GridLayoutManager(v.context, 1)

        recyclerView.adapter = articleListAdapter
        recyclerView.adapter
    }




}




