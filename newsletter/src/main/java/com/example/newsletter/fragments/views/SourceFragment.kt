package com.example.newsletter.fragments.views

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.example.newsletter.R
import com.example.newsletter.repositories.ArticleWebRepository
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class SourceFragment: Fragment() {
    private val repository = ArticleWebRepository()
    lateinit var chipGroup: ChipGroup

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_article_by_source, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        chipGroup = view.findViewById(R.id.filter_list_values)
        var p = parentFragment
        lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                val result = repository.getSources()
                result?.sources?.forEach {
                    val chip = Chip(chipGroup.context)
                    chip.setText(it.name)
                    chip.setTag(it.id)
                    chip.setCheckable(true)

                    activity?.runOnUiThread(java.lang.Runnable {
                        chipGroup.addView(chip)
                    })
                }
            }
        }
        chipGroup.setOnCheckedChangeListener(ChipGroup.OnCheckedChangeListener { chipGroup, i ->
            val chip: Chip = chipGroup.findViewById(i)
            Thread {
                (parentFragment as NewsFragment).getData( source = chip.tag as String)
            }.start()
        })
    }
}