package com.example.newsletter.fragments.views

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.view.children
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.example.newsletter.MainActivity
import com.example.newsletter.R
import com.example.newsletter.adapters.ArticleAdapter
import com.example.newsletter.adapters.ViewPagerAdapter
import com.example.newsletter.data.repositories.ArticleResponse

import com.example.newsletter.entity.Article
import com.example.newsletter.entity.Category
import com.example.newsletter.entity.Country
import com.example.newsletter.repositories.ArticleWebRepository
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CategoryFragment: Fragment() {

    private val repository = ArticleWebRepository()

    var selectedCategory: String = "general"
    var selectedCountry: String = "general"
    lateinit var chipGroup: ChipGroup
    private lateinit var listArticles: ArticleResponse

    private lateinit var v: View
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
//        binding = FragmentArticleByCategoryBinding.inflate(inflater, container, false)
//        return  binding.root
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_article_by_category, container, false)


        return v
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        chipGroup = view.findViewById(R.id.filter_list_values)

        lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                val categories = listOf<Category>(
                    Category("business","Business"),
                    Category("entertainment","Divertissement"),
                    Category("health","Santé"),
                    Category("science","Science"),
                    Category("sports","Sport"),
                    Category("technology","Technologie")
                )
                categories.forEach {
                    val chip: Chip = Chip(chipGroup.context)
                    chip.setText(it.label)
                    chip.setTag(it.name)
                    chip.setCheckable(true)
                    activity?.runOnUiThread(java.lang.Runnable {
                        chipGroup.addView(chip)
                    })
                }
            }
        }

        chipGroup.setOnCheckedChangeListener(ChipGroup.OnCheckedChangeListener { chipGroup, i ->
            val chip: Chip = chipGroup.findViewById(i)
            val category = chip.getTag() as String
            Thread {
                (parentFragment as NewsFragment).getData( category = category)
            }.start()


        })




    }


    companion object {

        fun newInstance(cat: String, country: String) = CategoryFragment().apply {
            this.selectedCategory = cat
            this.selectedCountry = country


        }
    }

}