package com.example.newsletter.fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.newsletter.R
import com.example.newsletter.data.models.ArticleViewModel
import com.example.newsletter.entity.Article
import com.example.newsletter.fragments.views.FavoritesFragment
import com.example.newsletter.fragments.views.NewsFragment
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.button.MaterialButton
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Use the [Article_detailsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ArticleDetailsFragment() : Fragment() {
    // TODO: Rename and change types of parameters
    var title: String = ""
    var author: String = ""
    var urlToImage: String = ""
    var url: String =""
    var content: String =""
    var publishedAt: String =""
//    private val viewModel: ArticleViewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            title = (it.getString("title")).toString()
            author = (it.getString("author")).toString()
            publishedAt = it.getString("publishedAt").toString()
            content = (it.getString("content")).toString()
            urlToImage = (it.getString("urlToImage")).toString()
            url = it.getString("url") as String
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_article_details, container, false)




    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var isFavorite: Boolean = false
        val textAuthor = view.findViewById<TextView>(R.id.author)
        val title_el = view.findViewById<TextView>(R.id.title)
        val articleViewBtn = view.findViewById<Button>(R.id.view_btn)
        val imageView: ImageView = view.findViewById<ImageView>(R.id.image)
        val textPublishedAt = view.findViewById<TextView>(R.id.published)
        val textContent = view.findViewById<TextView>(R.id.content)
        val fav: FloatingActionButton = view.findViewById(R.id.favourite_btn)
        val view_btn: MaterialButton = view.findViewById(R.id.view_btn)

        textAuthor.text = author
        title_el.text = title
        textContent.text = content
        articleViewBtn.tag =  url
        textPublishedAt.text = publishedAt
        textContent.text = content
//        fav.setOnClickListener {
//            viewModel.insert(article)
//            Snackbar.make(root, "Article sauvergardée", Snackbar.LENGTH_SHORT).show()
//        }
        view_btn.setOnClickListener{
            handleArticleClick(url)
        }



        Glide
            .with(view)
            .load(  urlToImage as String)
            .centerInside()
            .placeholder(R.drawable.image)
            .into(imageView);
        val toolbar : MaterialToolbar = view.findViewById(R.id.toolbar)
        toolbar.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.return_btn -> {
                    (parentFragment as FavoritesFragment).hideArticleDetailFragment()
                    // Handle favorite icon press
                    true
                }
                else -> false
            }
        }
    }

    fun handleArticleClick(url : String){
        val uriUrl: Uri = Uri.parse(url )
        val launchBrowser = Intent(Intent.ACTION_VIEW, uriUrl)
        startActivity(launchBrowser)
    }

    companion object {
        @JvmStatic
        fun newInstance(art : Article) =
            ArticleDetailsFragment().apply {
                arguments = Bundle().apply {
                    putString("title", art.title)
                    putString("content", art.content)
                    putString("publishedAt", art.publishedAt)
                    putString("author", art.author)
                    putString("url", art.url)
                    putString("urlToImage", art.urlToImage)
                }
            }
    }
}