package com.example.newsletter.fragments.views


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.example.newsletter.MainActivity
import com.example.newsletter.R
import com.example.newsletter.adapters.ArticleAdapter
import com.example.newsletter.adapters.ViewPagerAdapter
import com.example.newsletter.data.models.ArticleViewModel
import com.example.newsletter.data.repositories.ArticleResponse
import com.example.newsletter.repositories.ArticleWebRepository
import com.example.newsletter.entity.Article
import com.example.newsletter.fragments.ArticleDetailsFragment
import com.google.android.material.tabs.TabLayout

class NewsFragment : Fragment(R.layout.fragment_view_news) {
    private val repository = ArticleWebRepository()
    lateinit var viewModel: ArticleViewModel

    var selectedCategory: String = "general"
    var selectedCountry: String = ""
    var selectedSource: String = ""

    lateinit var tabLayout: TabLayout
    lateinit var viewPager: ViewPager

    private lateinit var listArticles: ArticleResponse

    private lateinit var viewArticleFrameLayout: FrameLayout

    private lateinit var v: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        viewModel = (activity as MainActivity).articleViewModel
        v = inflater.inflate(R.layout.fragment_view_news, container, false)

        val bundle = this.arguments
        if (bundle != null) {
            selectedCategory = bundle.getString("selectedCategory", "general")
            selectedCountry = bundle.getString("selectedCountry", "")
            selectedSource = bundle.getString("selectedSource", "")
        }
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Thread {
            getData(selectedCategory, selectedCountry, selectedSource)
        }.start()

        tabLayout = view.findViewById(R.id.tabLayout)
        viewPager = view.findViewById(R.id.viewPager)
        tabLayout.addTab(tabLayout.newTab().setText("Catégories"))
        tabLayout.addTab(tabLayout.newTab().setText("Pays"))
        tabLayout.addTab(tabLayout.newTab().setText("Sources"))

        tabLayout.tabGravity = TabLayout.GRAVITY_FILL
        val adapter = ViewPagerAdapter(childFragmentManager, tabLayout.tabCount)
        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,object:OnBackPressedCallback(true){
            override fun handleOnBackPressed() {
                hideArticleDetailFragment()
            }
        })

        viewArticleFrameLayout = view.findViewById(R.id.view_article)
       /* articleListAdapter.setOnItemClickListener {
            val bundle = Bundle().apply {
                putSerializable("article", it)
            }
            findNavController().navigate(
                R.id.action_listArticleFragment_to_articleFragment,
                bundle
            )
        }*/
    }


    //S'execute dans un thread secondeaire
    fun getData(category: String = "general", country: String = "", source: String = "") {
        val result =
            repository.getFilteredArticles(country = country, category = category, source = source)
        result?.articles?.let {
            activity?.runOnUiThread(java.lang.Runnable {
                bindData(it)
            })
        }
    }

    private fun bindData(result: List<Article>) {
        //afficher les données dans le recycler
        var recyclerView: RecyclerView = v.findViewById(R.id.article_card_list)


        var adapterRecycler = ArticleAdapter(result, viewModel)
        adapterRecycler.setOnViewBtnClickListener {
            val adf = ArticleDetailsFragment.newInstance(it)
            childFragmentManager.beginTransaction()
                .replace(R.id.view_article, adf)
                .commitAllowingStateLoss()
            viewArticleFrameLayout.setVisibility(View.VISIBLE)
            Log.d("test",it.title as String)
        }

        recyclerView.hasFixedSize()

        recyclerView.layoutManager = GridLayoutManager(v.context, 1)

        recyclerView.adapter = adapterRecycler
    }

    fun hideArticleDetailFragment(){
        viewArticleFrameLayout.setVisibility(View.GONE)
    }
    companion object {

        fun newInstance(cat: String = "", country: String = "", source: String = "") =
            NewsFragment().apply {
                this.selectedCategory = cat
                this.selectedCountry = country
                this.selectedSource = source

            }
    }
}
