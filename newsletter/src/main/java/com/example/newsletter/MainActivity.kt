package com.example.newsletter

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.newsletter.data.ArticleApplication
import com.example.newsletter.data.models.ArticleViewModel
import com.example.newsletter.data.models.ArticleViewModelFactory

import com.example.newsletter.fragments.views.AboutFragment
import com.example.newsletter.fragments.views.FavoritesFragment
import com.example.newsletter.fragments.views.NewsFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.floatingactionbutton.FloatingActionButton


class MainActivity : AppCompatActivity() {
    val news_fragment = NewsFragment()
    val favorites_fragment = FavoritesFragment()
    val about_fragment = AboutFragment()

    val articleViewModel: ArticleViewModel by viewModels {
        ArticleViewModelFactory((application as ArticleApplication).repository)
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if(savedInstanceState == null) { // initial transaction should be wrapped like this
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_view, news_fragment)
                .commitAllowingStateLoss()
        }
        val bottomNavigation = findViewById<BottomNavigationView>(R.id.bottom_navigation_view)
        bottomNavigation.setOnNavigationItemSelectedListener(navigationListeners)


    }


    private val navigationListeners = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {

            R.id.news_view_btn -> {
                // Respond to navigation item 1 click
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_view, news_fragment)
                    .commitAllowingStateLoss()
                true

            }
            R.id.favorites_view_btn -> {

                // Respond to navigation item 2 click
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_view, favorites_fragment)
                    .commitAllowingStateLoss()
                true
            }
            R.id.about_view_btn -> {
                // Respond to navigation item 2 click
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_view, about_fragment)
                    .commitAllowingStateLoss()
                true
            }
            else -> false
        }


    }

}
