package com.example.newsletter.adapters


import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.newsletter.R
import com.example.newsletter.data.ArticleApplication
import com.example.newsletter.data.models.ArticleViewModel
import com.example.newsletter.data.models.ArticleViewModelFactory
import com.example.newsletter.entity.Article
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar


class ArticleLiveAdapter(private val dataset: LiveData<List<Article>>, private val viewModel: ArticleViewModel):
    RecyclerView.Adapter<ArticleLiveAdapter.ViewHolder>() {


    class ViewHolder(val root: View) : RecyclerView.ViewHolder(root) {


        fun bind(item: Article, viewModel: ArticleViewModel) {
            //val textName =root.findViewById<TextView>(R.id.article_name)
            //val textAuthor = root.findViewById<TextView>(R.id.article_author)
            val title = root.findViewById<TextView>(R.id.title)
            val description = root.findViewById<TextView>(R.id.description)
            val articleViewBtn = root.findViewById<Button>(R.id.view_btn)
            val imageView: ImageView = root.findViewById<ImageView>(R.id.image)
            //val textPublishedAt = root.findViewById<TextView>(R.id.article_publishedAt)
            val textContent = root.findViewById<TextView>(R.id.content)
            val fav: FloatingActionButton = root.findViewById(R.id.favourite_btn)

            //textName.text = item.name
            //textAuthor.text = item.author
            title.text = item.title
            description.text = item.description
            //textContent.text = item.content
            articleViewBtn.tag = item.url
            //textPublishedAt.text = item.publishedAt
            //textContent.text = item.content
            fav.setOnClickListener {

                viewModel.deleteArticle(item)
                Snackbar.make(root, "Article supprimé des favoris", Snackbar.LENGTH_SHORT).show()

            }


            Glide
                .with(root)
                .load(item.urlToImage)
                .centerInside()
                .placeholder(R.drawable.image)
                .into(imageView);

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val rootView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_favorite_card, parent, false)

        return ViewHolder(rootView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        dataset.value?.get(position)?.toString()?.let { Log.d("dataset", it) }
        dataset.value?.get(position)?.let { holder.bind(it, viewModel) }
    }

    override fun getItemCount(): Int = dataset.value?.size!!


    private var onItemClickListener: ((Article) -> Unit)? = null

    fun setOnItemClickListener(listener: (Article) -> Unit) {
        onItemClickListener = listener
    }

}