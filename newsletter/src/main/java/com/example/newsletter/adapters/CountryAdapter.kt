package com.example.newsletter.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.newsletter.R
import com.example.newsletter.entity.Category
import com.example.newsletter.entity.Country
import com.google.android.material.chip.Chip

class CountryAdapter(private val dataset: List<Country>) :
    RecyclerView.Adapter<CountryAdapter.ViewHolder>() {
    class ViewHolder(val root: View) : RecyclerView.ViewHolder(root) {
        fun bind(item: Country) {
            val chip = root.findViewById<Chip>(R.id.chip)
            chip.text = item.name
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val rootView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_article_by_country, parent, false)
        return ViewHolder(rootView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataset[position])
    }

    override fun getItemCount() = dataset.size
}