package com.example.newsletter.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.newsletter.fragments.views.CategoryFragment
import com.example.newsletter.fragments.views.CountryFragment
import com.example.newsletter.fragments.views.SourceFragment

@Suppress("DEPRECATION")
internal  class ViewPagerAdapter (
    fm: FragmentManager,
    var totalTabs: Int
):

    FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                CategoryFragment()
            }
            1 -> {
                CountryFragment()
            }
            2 -> {
                SourceFragment()
            }
            else -> getItem(position)
        }
    }
    override fun getCount(): Int {
        return totalTabs
    }
}