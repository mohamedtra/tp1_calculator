package com.example.newsletter.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.newsletter.R
import com.example.newsletter.data.models.ArticleViewModel
import com.example.newsletter.entity.Article
import com.google.android.material.button.MaterialButton
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import java.security.AccessController.getContext


class ArticleAdapter(private val dataset: List<Article>, private val viewModel: ArticleViewModel):
    RecyclerView.Adapter<ArticleAdapter.ViewHolder>() {
    var listener: ((item: Article) -> Unit)? = null
    class ViewHolder(val root: View) : RecyclerView.ViewHolder(root) {

        var listener: ((item: Article) -> Unit)? = null
        fun setOnViewBtnClickListener(listener: (ite : Article) -> Unit) {
            this.listener = listener
        }



        fun bind(item: Article, viewModel: ArticleViewModel) {
            //val textName =root.findViewById<TextView>(R.id.article_name)
            //val textAuthor = root.findViewById<TextView>(R.id.article_author)
            val title = root.findViewById<TextView>(R.id.title)
            val description = root.findViewById<TextView>(R.id.description)
            val articleViewBtn = root.findViewById<Button>(R.id.view_btn)
            val imageView: ImageView = root.findViewById<ImageView>(R.id.image)
            //val textPublishedAt = root.findViewById<TextView>(R.id.article_publishedAt)
            val textContent = root.findViewById<TextView>(R.id.content)
            val fav: FloatingActionButton = root.findViewById(R.id.favourite_btn)
            val view_btn: MaterialButton = root.findViewById(R.id.view_btn)

            //textName.text = item.name
            //textAuthor.text = item.author
            title.text = item.title
            description.text = item.description
            //textContent.text = item.content
            articleViewBtn.tag = item.url
            //textPublishedAt.text = item.publishedAt
            //textContent.text = item.content



            Thread {
                var isFav: Boolean = viewModel.isFavorite(item.url)
                Log.d("isFavorite", isFav.toString())
                if(isFav){
                    fav.setImageDrawable(
                        ContextCompat.getDrawable(
                            fav.context,
                            R.drawable.ic_remove_circle
                        )
                    )
                }
            }.start()


            fav.setOnClickListener {

                Thread {
                        var isFav: Boolean = viewModel.isFavorite(item.url)
                        if (!isFav){
                            viewModel.insert(item)
                            Snackbar.make(root, "Article ajouté aux favoris", Snackbar.LENGTH_SHORT).show()
                            fav.setImageDrawable(
                                ContextCompat.getDrawable(
                                    fav.context,
                                    R.drawable.ic_remove_circle
                                )
                            )
                            
                    } else {

                        fav.setImageDrawable(
                            ContextCompat.getDrawable(
                                fav.context,
                                R.drawable.ic_favorite_white
                            )
                        )
                        viewModel.deleteArticle(item)
                        Snackbar.make(
                            itemView,
                            "Article supprimé des favoris",
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                }.start()

            }
            view_btn.setOnClickListener{
                listener?.invoke(item)
            }


            Glide
                .with(root)
                .load(item.urlToImage)
                .centerInside()
                .placeholder(R.drawable.image)
                .into(imageView);

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val rootView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_article_card, parent, false)
        var vh = ViewHolder(rootView)
        listener?.let { vh.setOnViewBtnClickListener(it) }
        return vh
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        dataset[position].let { holder.bind(it, viewModel) }
    }
    fun setOnViewBtnClickListener(listener: (ite : Article) -> Unit) {
        this.listener = listener
    }
    override fun getItemCount(): Int = dataset.size


    private var onItemClickListener: ((Article) -> Unit)? = null

}
