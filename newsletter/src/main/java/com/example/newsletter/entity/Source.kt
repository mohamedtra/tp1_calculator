package com.example.newsletter.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "source_table")
data class Source(
    @PrimaryKey(autoGenerate = true)
    var id: String,
    var name: String,
    var description: String?,
    var url: String?,
    var language: String?,
    var country: String?
)