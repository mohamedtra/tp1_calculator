package com.example.newsletter.entity

data class Editor (val name: String)