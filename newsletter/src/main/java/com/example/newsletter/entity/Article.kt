package com.example.newsletter.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "article_table")
data class Article  (
                    var author: String?,
                    var title: String?,
                    var description: String?,
                    @PrimaryKey
                    var url: String,
                    var urlToImage: String?,
                    var publishedAt: String?,
                    var content: String?)


// create table article_table(author TEXT, title TEXT, description TEXT, url TEXT primaryky