package com.example.newsletter.entity

data class Country (val name: String, val label:String, val id: Int)