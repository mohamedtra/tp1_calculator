package com.example.newsletter.data.repositories

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.example.newsletter.data.dao.ArticleDao
import com.example.newsletter.entity.Article
import kotlinx.coroutines.flow.Flow


// Declares the DAO as a private property in the constructor. Pass in the DAO
// instead of the whole database, because you only need access to the DAO
class ArticleRepository(private val articleDao: ArticleDao) {

    // Room executes all queries on a separate thread.
    // Observed Flow will notify the observer when the data has changed.
    val allArticles: LiveData<List<Article>> = articleDao.getAllArticles() //LiveData to change to Flow

    // By default Room runs suspend queries off the main thread, therefore, we don't need to
    // implement anything else to ensure we're not doing long running database work
    // off the main thread.
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(article: Article) {
        articleDao.insert(article)
    }

    @WorkerThread
    suspend fun deleteArticle(article: Article?){
        articleDao.deleteArticle(article)
    }

    @JvmName("getAllArticles1")
    fun getAllArticles(): LiveData<List<Article>> {
        return articleDao.getAllArticles()
    }

    fun isFavorite(url: String): Boolean {
        val res = articleDao.isFavorite(url)
        return res.value?.get(0) != null
    }


}
