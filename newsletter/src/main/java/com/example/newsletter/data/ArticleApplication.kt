package com.example.newsletter.data

import android.app.Application
import com.example.newsletter.data.repositories.ArticleRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class ArticleApplication : Application() {

    val applicationScope = CoroutineScope(SupervisorJob())
    // Using by lazy so the database and the repository are only created when they're needed
    // rather than when the application starts
    private val database by lazy {
        ArticleRoomDatabase.destroyDataBase(this, applicationScope)
        ArticleRoomDatabase.getDatabase(this, applicationScope)
    }
    val repository by lazy { ArticleRepository(database.articleDao()) }

}

