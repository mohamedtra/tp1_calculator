package com.example.newsletter.data.models

import androidx.lifecycle.*
import com.example.newsletter.data.repositories.ArticleRepository
import com.example.newsletter.entity.Article
import kotlinx.coroutines.launch
import com.example.newsletter.data.util.Resource
import retrofit2.Response


import java.lang.IllegalArgumentException

class ArticleViewModel(private val repository: ArticleRepository): ViewModel() {



    fun getAllArticles() = repository.allArticles


    fun insert(article: Article) = viewModelScope.launch {
        repository.insert(article)
    }

    fun deleteArticle(article: Article?) = viewModelScope.launch {
        repository.deleteArticle(article)
    }

    fun isFavorite(url: String): Boolean = repository.isFavorite(url)

 /*   private fun handleAllArticles(response: Response<List<Article>>): Resource<List<Article>>{
        if(response.isSuccesful){
            response.body().let {
                resultResponse -> return Resource.Success(resultResponse)
            }
        }
        return Resource.Error(response.message())
    }*/
}

class ArticleViewModelFactory(private val repository: ArticleRepository): ViewModelProvider.Factory{
    override fun <T: ViewModel> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(ArticleViewModel::class.java)){
            @Suppress("UNCHECKED_CAST")
            return ArticleViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}