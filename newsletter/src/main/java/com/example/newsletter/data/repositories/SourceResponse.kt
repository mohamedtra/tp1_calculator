package com.example.newsletter.data.repositories

import android.util.Log
import com.example.newsletter.entity.Source
import okhttp3.Request
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

data class SourceResponse (
    var sources : List<Source>
)