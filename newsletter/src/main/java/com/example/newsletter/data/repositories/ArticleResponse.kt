package com.example.newsletter.data.repositories

import android.util.Log
import com.example.newsletter.entity.Article
import okhttp3.Request
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

data class ArticleResponse (
    var status : String ,
    var totalResults : Int ,
    var articles : List<Article>
)