package com.example.newsletter.data.adapters



import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Glide.with
import com.example.newsletter.R
import com.example.newsletter.adapters.ArticleAdapter
import com.example.newsletter.data.models.ArticleViewModel
import com.example.newsletter.entity.Article
import com.google.android.material.button.MaterialButton
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar

class ArticleListAdapter(private val result: List<Article>, private val viewModel: ArticleViewModel) : RecyclerView.Adapter<ArticleListAdapter.ArticleViewHolder>() {

    var listener: ((item: Article?) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {

        val rootView = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_article_card, parent, false)

        var vh = ArticleViewHolder(rootView)

        listener?.let { vh.setOnViewBtnClickListener(it) }

        return vh
    }

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        result[position].let { holder.bind(it, viewModel) }

/*
        holder.itemView.apply {
            val articleTitle = this.findViewById<TextView>(R.id.title)
            val articleDescription = this.findViewById<TextView>(R.id.description)
            val articleContent = this.findViewById<TextView>(R.id.content)
            val articleViewBtn = this.findViewById<TextView>(R.id.view_btn)
            val articleImage = this.findViewById<ImageView>(R.id.image)
            Log.d("current url", current?.url.toString())

            //with(this).load(current.urlToImage).into(articleImage)
            articleContent?.text = current.content
            articleTitle?.text = current.title
            articleDescription?.text = current.description
            //publishedAt = current.publishedAt
            articleViewBtn?.tag = current.url
        }*/
    }





    class ArticleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var listener: ((item: Article?) -> Unit)? = null
        fun setOnViewBtnClickListener(listener: (ite : Article?) -> Unit) {
            this.listener = listener
        }

        fun bind(item: Article?, view: ArticleViewModel) {
            //val textName =itemView.findViewById<TextView>(R.id.article_name)
            //val textAuthor = itemView.findViewById<TextView>(R.id.article_author)
            val title: TextView? = itemView.findViewById<TextView>(R.id.title)
            val description: TextView? = itemView.findViewById<TextView>(R.id.description)
            //val articleViewBtn = itemView.findViewById<Button>(R.id.view_btn)
            val imageView: ImageView? = itemView.findViewById<ImageView>(R.id.image)
            //val textPublishedAt = view.findViewById<TextView>(R.id.article_publishedAt)
            val textContent = itemView.findViewById<TextView>(R.id.content)
            val fav: FloatingActionButton? = itemView.findViewById(R.id.favourite_btn)
            val view_btn: MaterialButton? = itemView.findViewById(R.id.view_btn)

            Log.d("article content", item?.content.toString())
            //textName.text = item.name
            //textAuthor.text = item.author
            title?.text = item?.title
            description?.text = item?.description
            //textContent.text = item.content
            view_btn?.tag = item?.url
            //textPublishedAt.text = item.publishedAt
            //textContent.text = item.content


            fav?.setOnClickListener {
                Thread {
                    fav.setImageDrawable(
                        ContextCompat.getDrawable(
                            fav.context,
                            R.drawable.ic_favorite_white
                        )
                    )
                    view.deleteArticle(item)
                    Snackbar.make(
                        itemView,
                        "Article supprimé des favoris",
                        Snackbar.LENGTH_SHORT
                    ).show()

                }.start()
                view.deleteArticle(item)
                Snackbar.make(itemView, "Article supprimé des favoris", Snackbar.LENGTH_SHORT).show()

            }

            view_btn?.setOnClickListener{
                Log.d("test","Clicked view")
                listener?.invoke(item)
            }



            if (imageView != null) {
                Glide
                    .with(itemView)
                    .load(item?.urlToImage)
                    .centerInside()
                    .placeholder(R.drawable.image)
                    .into(imageView)
            };
        }



        companion object {
            fun create(parent: ViewGroup): ArticleViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.fragment_favorite_card, parent, false)
                return ArticleViewHolder(view)
            }

        }
    }

    fun setOnViewBtnClickListener(listener: (ite : Article?) -> Unit) {
        this.listener = listener
    }

    private val differCallback = object: DiffUtil.ItemCallback<Article>() {
        override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean {
            return oldItem.url === newItem.url
        }

        override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, differCallback)

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

}
