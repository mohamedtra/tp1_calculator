package com.example.newsletter.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.newsletter.entity.Article

@Dao
interface ArticleDao {

    @Query("SELECT * FROM article_table")
    fun getAllArticles(): LiveData<List<Article>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(article: Article): Long

    @Query("DELETE FROM article_table")
    suspend fun deleteAll()

    @Delete
    suspend fun deleteArticle(article: Article?)

    @Query("SELECT * FROM article_table WHERE url = :url")
    fun isFavorite(url: String?): LiveData<List<Article>>


}