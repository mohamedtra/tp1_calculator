package com.example.newsletter.repositories

import com.example.newsletter.data.repositories.ArticleResponse
import com.example.newsletter.data.repositories.SourceResponse
import com.example.newsletter.entity.Article
import com.example.newsletter.interfaces.ArticleWebService
import com.example.newsletter.services.ArticleOnlineService
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Call
import retrofit2.HttpException

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.lang.Exception


class ArticleWebRepository {
    private val service: ArticleWebService

    init {
        val httpClient = OkHttpClient.Builder().apply {
            addApiInterceptor(this)
        }.build()
        val retrofit = Retrofit.Builder().apply {
            baseUrl("https://newsapi.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)

        }
            .build()
        service = retrofit.create(ArticleWebService::class.java)
    }

    fun list(): ArticleResponse? {
        val response = service.list().execute()
        return response.body()
    }

    fun getFilteredArticles(
        country: String = "general",
        category: String = "",
        source: String = ""
    ): ArticleResponse? {
//        if(country == "general" && category == "general") return service.list().execute().body()
//
//        if(category=="general") return service.getFilteredArticlesByCountry(country).execute().body()
//        if(country == "general") return service.getFilteredArticlesByCategory(category).execute().body()
        try {
            var req: Call<ArticleResponse>
            if (source != "") {
                req = service.getFilteredArticlesBySource(source.replace("*", "", false))
            } else
                if (country != "") req =
                    service.getFilteredArticlesByCountry(country.replace("*", "", false))
                else {
                    req = service.getFilteredArticlesByCategory(category)
                }
            val resp = req.execute()
            return resp.body()

        } catch (e: Exception) {
            print("hello")
        } catch (e: HttpException) {
            print("hello")
        }

        return ArticleResponse("error", 0, emptyList())
    }


    fun getSources(): SourceResponse? {
        val response = service.getSources().execute()
        return response.body()
    }

    private fun addApiInterceptor(builder: OkHttpClient.Builder) {
        builder.addInterceptor(object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                val original = chain.request()
                val originalHttpUrl = original.url
                val url = originalHttpUrl.newBuilder()
                    .addQueryParameter("apiKey", "511b3385569d41c6b410987d5ad47906")
                    .build()

                val requestBuilder = original.newBuilder()
                    .url(url)
                val request = requestBuilder.build()
                return chain.proceed(request)
            }
        })
    }
}

