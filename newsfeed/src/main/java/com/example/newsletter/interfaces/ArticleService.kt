package com.example.newsletter.interfaces



import com.example.newsletter.model.Article
import com.example.newsletter.repositories.ArticleResponse
import retrofit.Call
import retrofit.http.GET
import retrofit.http.Path
import retrofit.http.Query


interface ArticleService {
    @GET("top-headlines?country=us&apiKey=c4dcbdcf72654deb98c79170d0c94fb9")
    fun list(@Query("category") category: String): Call<ArticleResponse>

}