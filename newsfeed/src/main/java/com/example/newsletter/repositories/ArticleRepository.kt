package com.example.newsletter.repositories

import com.example.newsletter.interfaces.ArticleService
import com.example.newsletter.model.Article
import retrofit.GsonConverterFactory
import retrofit.Retrofit




class ArticleRepository {
    private val service: ArticleService
    init {
        val retrofit = Retrofit.Builder().apply {
            baseUrl("https://newsapi.org/v2/").
            addConverterFactory(GsonConverterFactory.create())
        }.build()
        service = retrofit.create(ArticleService::class.java)
    }

    fun list(category: String): ArticleResponse{

        val response = service.list(category = category).execute()
        return response.body() ?: ArticleResponse ( "Ok", 0, emptyList())
    }
}