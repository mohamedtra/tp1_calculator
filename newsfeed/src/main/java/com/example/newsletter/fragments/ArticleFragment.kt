package com.example.newsletter.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.fragment.app.Fragment

import com.example.newsletter.R
import com.example.newsletter.databinding.FragmentArticleBinding
import com.example.newsletter.databinding.FragmentCategoryItemsBinding


class ArticleFragment : Fragment() {

    private lateinit var url: String
    private lateinit var binding: FragmentArticleBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentArticleBinding.inflate(inflater, container, false)
        val url2 = view?.tag.toString()
        Log.i("url", url2)

        val rootView: View = inflater.inflate(R.layout.fragment_article, container, false)

        val view = rootView.findViewById<WebView>(R.id.article_web_view)
        view.settings.javaScriptEnabled = true
        view.loadUrl(url2)

        return binding.root
    }

    companion object {
        fun newInstance(url: String) = ArticleFragment().apply {
            this.url = url
        }
    }
}