package com.example.newsletter.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.view.children
import com.example.newsletter.MainActivity
import com.example.newsletter.R
import com.example.newsletter.databinding.FragmentCategoryItemsBinding
import com.example.newsletter.model.Category

class CategoryFragment : Fragment() {

    lateinit var binding: FragmentCategoryItemsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentCategoryItemsBinding.inflate(inflater, container, false)
        return  binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val categories = listOf<Category>(
            Category("general"),
            Category("business"),
            Category("sports"),
            Category("entertainment"),
            Category("health"),
            Category("science"),
            Category("technology")
        )

        binding.root.children.filter {
            it is Button
        }.forEach {
            it.setOnClickListener { view ->
                (activity as? MainActivity)?.changeArticleFragment(
                        ListArticleFragment.newInstance(
                                view.tag?.toString() ?: "general"
                        )
                )
                Log.d("category","category is :" + view.tag?.toString())
            }
        }
    }

    fun MainActivity.changeArticleFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().apply {
            //3) on remplace le contenu du container
            replace(R.id.fragment_articles, fragment)
            //4) on ajoute la transaction dans la backstack
            addToBackStack(null)
        }.commit()
        // 5) on commit la transaction
    }

    companion object {
        fun newInstance() = CategoryFragment().apply {
        }
    }
}
